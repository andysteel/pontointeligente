package com.gmail.andersoninfonet.pontointeligente.util

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.junit.Test
import org.junit.Assert

class SenhaUtilTest {
	
	private val senha = "123456"
	private val crypt = BCryptPasswordEncoder()
	
	@Test
	fun testGerarHashSenha() {
		val hash = crypt.encode(senha)
		Assert.assertTrue(crypt.matches(senha,hash))
	}
}