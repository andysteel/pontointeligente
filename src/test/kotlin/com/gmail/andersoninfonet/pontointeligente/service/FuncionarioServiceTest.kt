package com.gmail.andersoninfonet.pontointeligente.service

import com.gmail.andersoninfonet.pontointeligente.model.Empresa
import com.gmail.andersoninfonet.pontointeligente.model.Funcionario
import com.gmail.andersoninfonet.pontointeligente.model.enums.Perfil
import com.gmail.andersoninfonet.pontointeligente.repository.FuncionarioRepository
import com.gmail.andersoninfonet.pontointeligente.util.SenhaUtil
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.beans.factory.annotation.Autowired
import org.junit.Before
import kotlin.jvm.Throws
import java.lang.Exception
import org.mockito.BDDMockito
import org.mockito.Mockito
import org.junit.Test
import org.junit.Assert
import org.junit.Ignore

@RunWith(SpringRunner :: class)
@SpringBootTest
class FuncionarioServiceTest {
	
	private val EMAIL = "email@email.com"
	private val CPF = "12345678912"
	private val CNPJ = "51463645000100"
	
	@MockBean
	private val funcionarioRepository: FuncionarioRepository? = null
	
	@Autowired
	private val funcionarioService: FuncionarioService? = null
	
	@Before
	@Throws(Exception :: class)
	fun setUp() {
		BDDMockito.given(funcionarioRepository?.save(Mockito.any(Funcionario :: class.java))).willReturn(funcionario())
		BDDMockito.given(funcionarioRepository?.findOne(1L)).willReturn(funcionario())
		BDDMockito.given(funcionarioRepository?.findByEmail(EMAIL)).willReturn(funcionario())
		BDDMockito.given(funcionarioRepository?.findByCpf(CPF)).willReturn(funcionario())
	}
	
	@Ignore
	fun devePersistirFuncionario() {
		val funcionario: Funcionario? = funcionarioService?.persistir(funcionario())
		Assert.assertNotNull(funcionario)
	}
	
	@Ignore
	fun deveBuscarPorCPF() {
		val funcionario: Funcionario? = funcionarioService?.buscarPorCPF(CPF)
		Assert.assertNotNull(funcionario)
	}
	
	@Ignore
	fun deveBuscarPorEmail() {
		val funcionario: Funcionario? = funcionarioService?.buscarPorEmail(EMAIL)
		Assert.assertNotNull(funcionario)
	}
	
	@Test
	fun deveBuscarPorId() {
		val funcionario: Funcionario? = funcionarioService?.buscarPorId(1L)
		Assert.assertNotNull(funcionario)
	}
	
	private fun funcionario(): Funcionario = Funcionario("Nome", EMAIL, SenhaUtil().criptografar("123456"), CPF, Perfil.ROLE_USUARIO, empresa(),0.0,0.0f,0.0f)
	private fun empresa(): Empresa = Empresa("Razao Social", CNPJ)
}