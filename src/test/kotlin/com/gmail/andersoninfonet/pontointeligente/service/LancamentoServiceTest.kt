package com.gmail.andersoninfonet.pontointeligente.service

import org.springframework.boot.test.context.SpringBootTest
import org.junit.runner.RunWith
import org.springframework.test.context.junit4.SpringRunner
import com.gmail.andersoninfonet.pontointeligente.repository.LancamentoRepository
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.beans.factory.annotation.Autowired
import com.gmail.andersoninfonet.pontointeligente.model.Lancamento
import java.util.Date
import com.gmail.andersoninfonet.pontointeligente.model.enums.Tipo
import com.gmail.andersoninfonet.pontointeligente.model.Funcionario
import com.gmail.andersoninfonet.pontointeligente.util.SenhaUtil
import com.gmail.andersoninfonet.pontointeligente.model.Empresa
import com.gmail.andersoninfonet.pontointeligente.model.enums.Perfil
import org.junit.Before
import java.lang.Exception
import org.mockito.BDDMockito
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.PageImpl
import java.util.ArrayList
import org.mockito.Mockito
import org.junit.Test
import org.junit.Assert
import org.junit.Ignore

@RunWith(SpringRunner :: class)
@SpringBootTest
class LancamentoServiceTest {
	
	@MockBean
	private val lancamentoRepository: LancamentoRepository? = null
	
	@Autowired
	private val lancamentoService: LancamentoService? = null
	
	private val ID = 1L
	private val EMAIL = "email@email.com"
	private val CPF = "12345678912"
	private val CNPJ = "51463645000100"
	
	@Before
	@Throws(Exception :: class)
	fun setUp() {
		BDDMockito.given<Page<Lancamento>>(lancamentoRepository?.findByFuncionarioId(ID, PageRequest(0,10)))
				.willReturn(PageImpl(ArrayList<Lancamento>()))
		BDDMockito.given(lancamentoRepository?.findOne(ID)).willReturn(lancamento())
		BDDMockito.given(lancamentoRepository?.save(Mockito.any(Lancamento :: class.java))).willReturn(lancamento())
	}
	
	@Ignore
	fun deveBuscarLancamentoPorFuncionarioId() {
		val lancamento: Page<Lancamento>? = lancamentoService?.buscarPorFuncionarioId(ID, PageRequest(0,10))
		Assert.assertNotNull(lancamento)
	}
	
	@Ignore
	fun deveBuscarLancamentoPorId() {
		val lancamento: Lancamento? = lancamentoService?.buscarPorId(ID)
		Assert.assertNotNull(lancamento)
	}
	
	@Test
	fun devePersistirLancamento() {
		val lancamento = lancamentoService?.persistir(lancamento())
		Assert.assertNotNull(lancamento)
	}
	
	private fun lancamento(): Lancamento = Lancamento( Date(), Tipo.INICIO_TRABALHO, funcionario(), "teste descricao", "teste localizado")
	private fun funcionario(): Funcionario = Funcionario("Nome", EMAIL, SenhaUtil().criptografar("123456"), CPF, Perfil.ROLE_USUARIO, empresa(),0.0,0.0f,0.0f)
	private fun empresa(): Empresa = Empresa("Razao Social", CNPJ)
}