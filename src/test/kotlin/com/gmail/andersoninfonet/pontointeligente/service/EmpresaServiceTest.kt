package com.gmail.andersoninfonet.pontointeligente.service

import org.junit.runner.RunWith
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import com.gmail.andersoninfonet.pontointeligente.repository.EmpresaRepository
import org.springframework.boot.test.mock.mockito.MockBean
import org.junit.Before
import kotlin.jvm.Throws
import org.mockito.BDDMockito
import com.gmail.andersoninfonet.pontointeligente.model.Empresa
import org.springframework.beans.factory.annotation.Autowired
import org.junit.Test
import org.junit.Assert

@RunWith(SpringRunner :: class)
@SpringBootTest
class EmpresaServiceTest {

	@Autowired
	val empresaService: EmpresaService? = null
	
	@MockBean
	private val empresaRepository: EmpresaRepository? = null
	
	private val CNPJ = "51463645000100"
	
	@Before
	@Throws(Exception :: class)
	fun setUP() {
		BDDMockito.given(empresaRepository?.findByCnpj(CNPJ)).willReturn(empresa())
		BDDMockito.given(empresaRepository?.save(empresa())).willReturn(empresa())
	}
	
	@Test
	fun deveBuscarEmpresaPorCNPJ() {
		val empresa: Empresa? = empresaService?.buscarPorCnpj(CNPJ)
		Assert.assertNotNull(empresa)
	}
	
	@Test
	fun devePersistirEmpresa() {
		val empresa: Empresa? = empresaService?.persistir(empresa())
		Assert.assertNotNull(empresa)
	}
	
	private fun empresa(): Empresa = Empresa("Razao Social", CNPJ)
}