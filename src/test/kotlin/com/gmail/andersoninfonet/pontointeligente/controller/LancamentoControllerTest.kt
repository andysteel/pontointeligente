package com.gmail.andersoninfonet.pontointeligente.controller

import org.junit.runner.RunWith
import org.springframework.test.context.junit4.SpringRunner
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.test.web.servlet.MockMvc
import org.springframework.beans.factory.annotation.Autowired
import com.gmail.andersoninfonet.pontointeligente.service.LancamentoService
import org.springframework.boot.test.mock.mockito.MockBean
import com.gmail.andersoninfonet.pontointeligente.service.FuncionarioService
import com.gmail.andersoninfonet.pontointeligente.model.enums.Tipo
import java.util.Date
import java.text.SimpleDateFormat
import org.junit.Test
import java.lang.Exception
import com.gmail.andersoninfonet.pontointeligente.model.Lancamento
import com.gmail.andersoninfonet.pontointeligente.model.Funcionario
import com.gmail.andersoninfonet.pontointeligente.util.SenhaUtil
import com.gmail.andersoninfonet.pontointeligente.model.enums.Perfil
import com.fasterxml.jackson.core.JsonProcessingException
import com.gmail.andersoninfonet.pontointeligente.dto.LancamentoDTO
import com.fasterxml.jackson.databind.ObjectMapper
import org.mockito.BDDMockito
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.junit.Ignore
import com.gmail.andersoninfonet.pontointeligente.model.Empresa

@RunWith(SpringRunner::class)
@SpringBootTest
@AutoConfigureMockMvc
class LancamentoControllerTest {
	
	@Autowired
	private val mvc: MockMvc? = null
	
	@MockBean
	private val lancamentoService: LancamentoService? = null
	
	@MockBean
	private val funcionarioService: FuncionarioService? = null
	
	private val urlBase: String = "/api/v1/lancamentos/"
	private val funcionarioId: String = "4"
	private val lancamentoId: String = "11"
	private val tipo: String = Tipo.INICIO_TRABALHO.name
	private val data: Date = Date()
	
	private val dateFormat: SimpleDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
	
	@Ignore
	@Throws(Exception::class)
	@WithMockUser
	fun deveCadastrarLancamento() {
		val lancamento: Lancamento = obterLancamento()
		BDDMockito.given<Funcionario>(funcionarioService?.buscarPorId(funcionarioId.toLong())).willReturn(funcionario())
		BDDMockito.given(lancamentoService?.persistir(obterLancamento())).willReturn(lancamento)
		
		mvc!!.perform(MockMvcRequestBuilders.post(urlBase)
				.content(obterJsonRequisicaoPost())
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isOk)
				.andExpect(MockMvcResultMatchers.jsonPath("$.data.tipo").value(tipo))
				.andExpect(MockMvcResultMatchers.jsonPath("$.data.data").value(dateFormat.format(data)))
				.andExpect(MockMvcResultMatchers.jsonPath("$.data.funcionarioId").value(funcionarioId))
				.andExpect(MockMvcResultMatchers.jsonPath("$.erros").isEmpty)
	}
	
	@Ignore
	@Throws(Exception::class)
	@WithMockUser
	fun deveCadastrarLancamentoFuncionarioIdIvalido() {
		BDDMockito.given<Funcionario>(funcionarioService?.buscarPorId(funcionarioId.toLong())).willReturn(null)
		mvc!!.perform(MockMvcRequestBuilders.post(urlBase)
				.content(obterJsonRequisicaoPost())
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isBadRequest)
				.andExpect(MockMvcResultMatchers.jsonPath("$.erros").value("Funcionario não encontrado. ID inexistente."))
				.andExpect(MockMvcResultMatchers.jsonPath("$.data").isEmpty)
	}
	
	@Test
	@Throws(Exception::class)
	@WithMockUser(username="admin@admin.com", roles = arrayOf("ADMIN"))
	fun deveRemoverUmLancamento() {
		BDDMockito.given<Lancamento>(lancamentoService?.buscarPorId(lancamentoId.toLong())).willReturn(obterLancamento())
		mvc!!.perform(MockMvcRequestBuilders.post(urlBase+lancamentoId)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(MockMvcResultMatchers.status().isOk)
	}
	
	@Throws(JsonProcessingException::class)
	private fun obterJsonRequisicaoPost(): String {
		val lancamentoDTO: LancamentoDTO = LancamentoDTO(dateFormat.format(data),tipo,"descricao","localizacao",funcionarioId)
		val mapper = ObjectMapper()
		return mapper.writeValueAsString(lancamentoDTO)
	}
	
	private fun obterLancamento(): Lancamento = Lancamento(data,Tipo.valueOf(tipo),funcionario(),"descrição","localizacao")
	
	private fun funcionario(): Funcionario = Funcionario("Andy","email@email.com",SenhaUtil().criptografar("123456"),
														"09963009700",Perfil.ROLE_USUARIO,Empresa())
}