package com.gmail.andersoninfonet.pontointeligente.model.enums

enum class Tipo {
	INICIO_TRABALHO,
	TERMINO_TRABALHO,
	INICIO_ALMOCO,
	TERMINO_ALMOCO,
	INICIO_PAUSA,
	TERMINO_PAUSA
}