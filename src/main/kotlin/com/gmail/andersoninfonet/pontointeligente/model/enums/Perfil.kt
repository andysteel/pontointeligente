package com.gmail.andersoninfonet.pontointeligente.model.enums

enum class Perfil {
	ROLE_ADMIN,
	ROLE_USUARIO
}