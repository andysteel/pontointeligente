package com.gmail.andersoninfonet.pontointeligente.model

import javax.persistence.Entity
import javax.persistence.Table
import javax.persistence.Id
import javax.persistence.Column
import javax.persistence.SequenceGenerator
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import java.io.Serializable

@Entity
@Table(name = "empresa", schema = "admin")
data class Empresa (
	
	@Column(name = "razao_social", nullable = false)
	val razaoSocial: String,
	
	@Column(name = "cnpj", nullable = false)
	val cnpj: String
) {
	constructor() : this(razaoSocial = "", cnpj = "")
	
	@Id
	@SequenceGenerator(name = "seq_empresa", sequenceName = "seq_empresa", schema = "admin", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_empresa")
	var id: Long? = null
}