package com.gmail.andersoninfonet.pontointeligente.model

import javax.persistence.Entity
import javax.persistence.Table
import java.util.Date
import com.gmail.andersoninfonet.pontointeligente.model.enums.Tipo
import javax.persistence.Id
import javax.persistence.SequenceGenerator
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Column
import javax.persistence.Temporal
import javax.persistence.TemporalType
import javax.persistence.Enumerated
import javax.persistence.EnumType
import javax.persistence.ManyToOne
import javax.persistence.FetchType
import javax.persistence.CascadeType
import java.io.Serializable

@Entity
@Table(name = "lancamento", schema = "admin")
data class Lancamento (
		
	@Temporal(TemporalType.DATE)
	@Column(name = "data", nullable = false)
	val data: Date? = null,
	
	@Enumerated(EnumType.STRING)
	@Column(name = "tipo")
	val tipo: Tipo? = null,
	
	@ManyToOne(fetch = FetchType.LAZY)
	val funcionario: Funcionario?,
	
	@Column(name = "descricao", nullable = false)
	val descricao: String? = "",
	
	@Column(name = "localizacao", nullable = false)
	val localizacao: String? = ""

) {
	constructor() : this(data = null, tipo = null, funcionario = null, descricao = "", localizacao = "")
	
	@Id
	@SequenceGenerator(name = "seq_lancamento", sequenceName = "seq_lancamento", schema = "admin", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_lancamento")
	var id: Long? = null
}
	
	
