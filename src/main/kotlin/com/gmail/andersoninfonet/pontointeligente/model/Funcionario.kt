package com.gmail.andersoninfonet.pontointeligente.model

import javax.persistence.Entity
import javax.persistence.Table
import javax.persistence.Id
import javax.persistence.SequenceGenerator
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import com.gmail.andersoninfonet.pontointeligente.model.enums.Perfil
import javax.persistence.Column
import javax.persistence.Enumerated
import javax.persistence.EnumType
import javax.persistence.ManyToOne
import javax.persistence.CascadeType
import javax.persistence.FetchType
import java.io.Serializable

@Entity
@Table(name = "funcionario", schema = "admin")
data class Funcionario (
	
	@Column(name = "nome", nullable = false)
	val nome: String? = null,
	
	@Column(name = "email", nullable = false)
	val email: String,
	
	@Column(name = "senha", nullable = false)
	val senha: String? = null,
	
	@Column(name = "cpf", nullable = false)
	val cpf: String,
	
	@Enumerated(EnumType.STRING)
	@Column(name = "perfil", nullable = false)
	val perfil: Perfil? = null,
	
	@ManyToOne(fetch = FetchType.LAZY)
	val empresa: Empresa?,
	
	@Column(name = "valor_hora")
	val valorHora: Double? = 0.0,
	
	@Column(name = "qtd_horas_trabalho_dia")
	val qtdHorasTrabalhoDia: Float? = 0.0f,
	
	@Column(name = "qtd_horas_almoco")
	val qtdHorasAlmoco: Float? = 0.0f

) {
	constructor(): this(nome = "", email = "", senha = "", cpf = "",
						perfil = null, empresa = null, valorHora = 0.0,
						qtdHorasTrabalhoDia = 0.0f, qtdHorasAlmoco = 0.0f)
	
	@Id
	@SequenceGenerator(name = "seq_funcionario", sequenceName = "seq_funcionario", schema = "admin", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_funcionario")
	var id: Long? = null
}