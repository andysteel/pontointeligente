package com.gmail.andersoninfonet.pontointeligente.security

import com.gmail.andersoninfonet.pontointeligente.service.FuncionarioService
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.stereotype.Service
import com.gmail.andersoninfonet.pontointeligente.model.Funcionario
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.security.core.userdetails.UserDetails

@Service
class FuncionarioDetailsService(val funcionarioService: FuncionarioService) : UserDetailsService {
	
	override fun loadUserByUsername(username: String?): UserDetails? {
		if(username != null) {
			val funcionario: Funcionario? = funcionarioService.buscarPorEmail(username)
			if(funcionario != null) {
				return FuncionarioPrincipal(funcionario)
			}
		}
		throw UsernameNotFoundException(username)
	}
}