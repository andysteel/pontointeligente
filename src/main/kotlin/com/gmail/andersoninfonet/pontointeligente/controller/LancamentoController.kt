package com.gmail.andersoninfonet.pontointeligente.controller

import org.springframework.web.bind.annotation.RestController
import org.springframework.web.bind.annotation.RequestMapping
import com.gmail.andersoninfonet.pontointeligente.service.LancamentoService
import com.gmail.andersoninfonet.pontointeligente.service.FuncionarioService
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import com.gmail.andersoninfonet.pontointeligente.response.Response
import com.gmail.andersoninfonet.pontointeligente.dto.LancamentoDTO
import org.springframework.validation.BindingResult
import javax.validation.Valid
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.PostMapping
import com.gmail.andersoninfonet.pontointeligente.model.Lancamento
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.security.access.prepost.PreAuthorize

@RestController
@RequestMapping("/api/v1/lancamentos")
open class LancamentoController (val lancamentoService: LancamentoService,
							val funcionarioService: FuncionarioService) {
	
	@Value("\${paginacao.qtd_por_pagina}")
	val qtdPorPagina: Int = 15
	
	@PostMapping
	open fun adicionar(@Valid @RequestBody lancamentoDTO: LancamentoDTO,
				  result: BindingResult): ResponseEntity<Response<LancamentoDTO>> {
		val response = Response<LancamentoDTO>()
		funcionarioService.validarFuncionario(lancamentoDTO, result)
		
		if(result.hasErrors()) {
			for(erro in result.allErrors) {
				response.erros.add(erro.defaultMessage)
			}
			return ResponseEntity.badRequest().body(response)
		}
		
		val lancamento: Lancamento = lancamentoService.converterDTOParaLancamento(lancamentoDTO, result)
		lancamentoService.persistir(lancamento)
		response.data = lancamentoService.converteLancamentoDTO(lancamento)
		return ResponseEntity.ok(response)
	}
	
	@GetMapping(value = "/{id}")
	open fun listarPorId(@PathVariable("id")id: String): ResponseEntity<Response<LancamentoDTO>> {
		val response: Response<LancamentoDTO> = Response<LancamentoDTO>()
		val lancamento: Lancamento? = lancamentoService.buscarPorId(id.toLong())
		
		if(lancamento == null) {
			response.erros.add("Lançamento não encontrado para o ID $id")
			return ResponseEntity.badRequest().body(response)
		}
		
		response.data = lancamentoService.converteLancamentoDTO(lancamento)
		return ResponseEntity.ok(response)
	}
	
	@GetMapping(value = "/funcionario/{funcionarioId}")
	open fun listarPorFuncionarioID(@PathVariable("funcionarioId")funcionarioId: String,
							   @RequestParam(value = "pag", defaultValue = "0")pag: Int,
							   @RequestParam(value = "ord", defaultValue = "id")ord: String,
							   @RequestParam(value = "dir", defaultValue = "DESC")dir: String): ResponseEntity<Response<Page<LancamentoDTO>>> {
		val response: Response<Page<LancamentoDTO>> = Response<Page<LancamentoDTO>>()
		val pageRequest: PageRequest = PageRequest(pag, qtdPorPagina,Sort.Direction.fromString(dir),ord)
		val lancamentos: Page<Lancamento> = lancamentoService.buscarPorFuncionarioId(funcionarioId.toLong(),pageRequest)
		val lancamentosDTO: Page<LancamentoDTO> = lancamentos.map { lancamento -> lancamentoService.converteLancamentoDTO(lancamento) }
		
		response.data = lancamentosDTO
		return ResponseEntity.ok(response)
	}
	
	@PutMapping(value = "/{id}")
	open fun atualizar(@PathVariable("id")id: String, @Valid @RequestBody lancamentoDTO: LancamentoDTO,
				  result: BindingResult ): ResponseEntity<Response<LancamentoDTO>> {
		val response: Response<LancamentoDTO> = Response<LancamentoDTO>()
		funcionarioService.validarFuncionario(lancamentoDTO, result)
		lancamentoDTO.id = id
		val lancamento: Lancamento = lancamentoService.converterDTOParaLancamento(lancamentoDTO, result)
		
		if(result.hasErrors()) {
			for(erro in result.allErrors) {
				response.erros.add(erro.defaultMessage)
			}
			return ResponseEntity.badRequest().body(response)
		}
		
		lancamentoService.persistir(lancamento)
		response.data = lancamentoService.converteLancamentoDTO(lancamento)
		return ResponseEntity.ok(response)
	}
	
	@DeleteMapping(value = "/{id}")
	@PreAuthorize("hasAnyRole('ADMIN')")
	open fun remover(@PathVariable("id")id: String): ResponseEntity<Response<String>> {
		val response: Response<String> = Response<String>()
		var lancamento: Lancamento? = lancamentoService.buscarPorId(id.toLong())
		
		if(lancamento == null) {
			response.erros.add("Erro ao remover lancamento. Registro não encontrado para o ID $id")
			return ResponseEntity.badRequest().body(response)
		}
		
		lancamentoService.remover(id.toLong())
		return ResponseEntity.ok(Response<String>())
	}
}