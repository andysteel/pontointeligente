package com.gmail.andersoninfonet.pontointeligente.controller

import org.springframework.web.bind.annotation.RestController
import org.springframework.web.bind.annotation.RequestMapping
import com.gmail.andersoninfonet.pontointeligente.service.EmpresaService
import com.gmail.andersoninfonet.pontointeligente.service.FuncionarioService
import org.springframework.web.bind.annotation.PostMapping
import com.gmail.andersoninfonet.pontointeligente.dto.CadastroPfDTO
import org.springframework.validation.BindingResult
import javax.validation.Valid
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.http.ResponseEntity
import com.gmail.andersoninfonet.pontointeligente.response.Response
import com.gmail.andersoninfonet.pontointeligente.model.Empresa
import org.springframework.validation.ObjectError
import com.gmail.andersoninfonet.pontointeligente.model.Funcionario
import com.gmail.andersoninfonet.pontointeligente.util.SenhaUtil
import com.gmail.andersoninfonet.pontointeligente.model.enums.Perfil

@RestController
@RequestMapping("\${url_base}/cadastrar-pf")
class CadastroPfController(val empresaService:EmpresaService, val funcionarioService:FuncionarioService) {
	
	@PostMapping
	fun cadastrar(@Valid @RequestBody cadastroPfDto:CadastroPfDTO,
				  result:BindingResult): ResponseEntity<Response<CadastroPfDTO>> {
		val response: Response<CadastroPfDTO> = Response<CadastroPfDTO>()
		
		val empresa: Empresa? = empresaService.buscarPorCnpj(cadastroPfDto.cnpj)
		funcionarioService.validarDadosExistentes(cadastroPfDto,empresa,result)
		
		if(result.hasErrors()) {
			for(erro in result.allErrors) response.erros.add(erro.defaultMessage)
			return ResponseEntity.badRequest().body(response)
		}
		
		val funcionario: Funcionario = funcionarioService.converterDtoParaFuncionario(cadastroPfDto,empresa!!)
		funcionarioService.persistir(funcionario)
		response.data = funcionarioService.converterCadastroPfDto(funcionario,empresa)
		return ResponseEntity.ok(response)
	}
	
}