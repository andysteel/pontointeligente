package com.gmail.andersoninfonet.pontointeligente.controller

import org.springframework.web.bind.annotation.RestController
import org.springframework.web.bind.annotation.RequestMapping
import com.gmail.andersoninfonet.pontointeligente.service.EmpresaService
import com.gmail.andersoninfonet.pontointeligente.model.Empresa
import com.gmail.andersoninfonet.pontointeligente.dto.EmpresaDTO
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.http.ResponseEntity
import com.gmail.andersoninfonet.pontointeligente.response.Response

@RestController
@RequestMapping("\${url_base}/empresas")
class EmpresaController(val empresaService: EmpresaService) {
	
	@GetMapping("/cnpj/{cnpj}")
	fun buscarPorCnpj(@PathVariable("cnpj") cnpj:String): ResponseEntity<Response<EmpresaDTO>> {
		val response: Response<EmpresaDTO> = Response<EmpresaDTO>()
		val empresa: Empresa? = empresaService.buscarPorCnpj(cnpj)
		if(empresa == null) {
			response.erros.add("Empresa não encontrada para o cnpj $cnpj")
			return ResponseEntity.badRequest().body(response)
		}
		
		response.data = empresaService.converterEmpresaDTO(empresa)
		return ResponseEntity.ok(response)
	}
	
}