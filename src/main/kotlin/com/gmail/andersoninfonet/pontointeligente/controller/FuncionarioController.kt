package com.gmail.andersoninfonet.pontointeligente.controller

import org.springframework.web.bind.annotation.RestController
import org.springframework.web.bind.annotation.RequestMapping
import com.gmail.andersoninfonet.pontointeligente.service.FuncionarioService
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.PathVariable
import com.gmail.andersoninfonet.pontointeligente.dto.FuncionarioDTO
import javax.validation.Valid
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.validation.BindingResult
import org.springframework.http.ResponseEntity
import com.gmail.andersoninfonet.pontointeligente.response.Response
import com.gmail.andersoninfonet.pontointeligente.model.Funcionario
import org.springframework.validation.ObjectError
import com.gmail.andersoninfonet.pontointeligente.util.SenhaUtil

@RestController
@RequestMapping("\${url_base}/funcionarios")
class FuncionarioController(val funcionarioService: FuncionarioService) {
	
	@PutMapping("/{id}")
	fun atualizar(@PathVariable("id") id: String,@Valid @RequestBody funcionarioDTO: FuncionarioDTO,
				  result: BindingResult): ResponseEntity<Response<FuncionarioDTO>> {
		val response: Response<FuncionarioDTO> = Response<FuncionarioDTO>()
		val funcionario: Funcionario? = funcionarioService.buscarPorId(id.toLong())
		
		if(funcionario == null) {
			result.addError(ObjectError("funcionario","Funcionario não encontrado."))
		}
		
		if(result.hasErrors()) {
			for(erro in result.allErrors) response.erros.add(erro.defaultMessage)
			return ResponseEntity.badRequest().body(response)
		}
		
		val funcAtualizar: Funcionario = funcionarioService.atualizarDadosFuncionario(funcionario!!, funcionarioDTO)
		funcionarioService.persistir(funcAtualizar)
		response.data = funcionarioService.converterFuncionarioDTO(funcAtualizar)
		
		return ResponseEntity.ok(response)
	}
	
}