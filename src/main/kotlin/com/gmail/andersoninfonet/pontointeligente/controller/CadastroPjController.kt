package com.gmail.andersoninfonet.pontointeligente.controller

import org.springframework.web.bind.annotation.RestController
import org.springframework.web.bind.annotation.RequestMapping
import com.gmail.andersoninfonet.pontointeligente.service.EmpresaService
import com.gmail.andersoninfonet.pontointeligente.service.FuncionarioService
import org.springframework.http.ResponseEntity
import com.gmail.andersoninfonet.pontointeligente.response.Response
import com.gmail.andersoninfonet.pontointeligente.dto.CadastroPjDTO
import org.springframework.web.bind.annotation.PostMapping
import javax.validation.Valid
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.validation.BindingResult
import com.gmail.andersoninfonet.pontointeligente.model.Empresa
import org.springframework.validation.ObjectError
import com.gmail.andersoninfonet.pontointeligente.model.Funcionario
import com.gmail.andersoninfonet.pontointeligente.util.SenhaUtil
import com.gmail.andersoninfonet.pontointeligente.model.enums.Perfil

@RestController
@RequestMapping("\${url_base}/cadastrar-pj")
class CadastroPjController(val empresaService: EmpresaService,val funcionarioService: FuncionarioService) {

	@PostMapping
	fun cadastrar(@Valid @RequestBody cadastroPjDTO: CadastroPjDTO,
				  result: BindingResult):ResponseEntity<Response<CadastroPjDTO>> {
		val response: Response<CadastroPjDTO> = Response<CadastroPjDTO>()
		
		empresaService.validarDadosExistentes(cadastroPjDTO,result)
		if(result.hasErrors()) {
			for(erro in result.allErrors) response.erros.add(erro.defaultMessage)
			return ResponseEntity.badRequest().body(response)
		}
		
		val empresa: Empresa = empresaService.converterDtoParaEmpresa(cadastroPjDTO)
		empresaService.persistir(empresa)
		
		val funcionario: Funcionario = empresaService.converterDtoParaFuncionario(cadastroPjDTO,empresa)
		funcionarioService.persistir(funcionario)
		
		response.data = empresaService.converterCadastroPjDto(funcionario,empresa)
		return ResponseEntity.ok(response)
	}
	
}