package com.gmail.andersoninfonet.pontointeligente.config

import com.mchange.v2.c3p0.ComboPooledDataSource
import javax.sql.DataSource
import java.lang.Exception

class ProdDataSource : DataSourceConfig{
	
	@Throws(Exception :: class)
	override fun dataSource(): DataSource {
		val ds: ComboPooledDataSource = ComboPooledDataSource()
				ds.setDriverClass("org.postgresql.Driver")
				ds.setJdbcUrl("jdbc:postgresql://localhost:5432/ponto_inteligente")
				ds.setUser("root")
				ds.setPassword("123456")
				ds.setInitialPoolSize(2)
				ds.setMinPoolSize(2)
				ds.setMaxPoolSize(5)
				ds.setAcquireIncrement(2)
		return ds
	}
}