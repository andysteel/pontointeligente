package com.gmail.andersoninfonet.pontointeligente.config

import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.context.annotation.ComponentScan
import org.springframework.transaction.annotation.EnableTransactionManagement
import org.springframework.transaction.PlatformTransactionManager
import org.springframework.orm.jpa.JpaTransactionManager
import javax.persistence.EntityManagerFactory
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean
import org.springframework.context.annotation.Bean
import javax.sql.DataSource

@Configuration
@EnableJpaRepositories("com.gmail.andersoninfonet.pontointeligente.repository")
@ComponentScan("com.gmail.andersoninfonet.pontointeligente")
@EnableTransactionManagement
open class DataConfig {
	
	@Bean
	public open fun dataSource(): DataSource = ProdDataSource().dataSource()
	
	@Bean
	public open fun entityManagerFactory(): EntityManagerFactory {
		val vendorAdapter = HibernateJpaVendorAdapter()
		vendorAdapter.setGenerateDdl(true)
		val factory = LocalContainerEntityManagerFactoryBean()
		factory.setJpaVendorAdapter(vendorAdapter)
		factory.setPackagesToScan("com.gmail.andersoninfonet.pontointeligente")
		factory.setDataSource(dataSource())
		factory.afterPropertiesSet()
		return factory.getObject()
	}
	
	@Bean
	public open fun transactionManager(): PlatformTransactionManager {
		val txManager: JpaTransactionManager = JpaTransactionManager()
		txManager.setEntityManagerFactory(entityManagerFactory())
		return txManager		
	}
}