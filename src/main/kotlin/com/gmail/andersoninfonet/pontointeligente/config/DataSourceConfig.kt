package com.gmail.andersoninfonet.pontointeligente.config

import javax.sql.DataSource

interface DataSourceConfig {
	
	fun dataSource(): DataSource
}