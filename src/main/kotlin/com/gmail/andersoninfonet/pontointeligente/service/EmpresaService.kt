package com.gmail.andersoninfonet.pontointeligente.service

import com.gmail.andersoninfonet.pontointeligente.model.Empresa
import com.gmail.andersoninfonet.pontointeligente.dto.EmpresaDTO
import com.gmail.andersoninfonet.pontointeligente.dto.CadastroPjDTO
import org.springframework.validation.BindingResult
import com.gmail.andersoninfonet.pontointeligente.model.Funcionario

interface EmpresaService {

	fun buscarPorCnpj(cnpj: String): Empresa?
	
	fun persistir(empresa: Empresa): Empresa
	
	fun converterEmpresaDTO(empresa: Empresa): EmpresaDTO
	
	fun validarDadosExistentes(cadastroPjDto: CadastroPjDTO, result: BindingResult)
	
	fun converterDtoParaEmpresa(cadastroPjDTO: CadastroPjDTO): Empresa
	
	fun converterDtoParaFuncionario(cadastroPjDTO: CadastroPjDTO,empresa: Empresa): Funcionario
	
	fun converterCadastroPjDto(funcionario: Funcionario, empresa: Empresa): CadastroPjDTO
}