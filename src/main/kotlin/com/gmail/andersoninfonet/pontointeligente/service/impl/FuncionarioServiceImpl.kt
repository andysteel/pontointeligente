package com.gmail.andersoninfonet.pontointeligente.service.impl

import com.gmail.andersoninfonet.pontointeligente.service.FuncionarioService
import com.gmail.andersoninfonet.pontointeligente.model.Funcionario
import org.springframework.stereotype.Service
import com.gmail.andersoninfonet.pontointeligente.repository.FuncionarioRepository
import com.gmail.andersoninfonet.pontointeligente.dto.LancamentoDTO
import org.springframework.validation.BindingResult
import org.springframework.validation.ObjectError
import org.springframework.transaction.annotation.Transactional
import com.gmail.andersoninfonet.pontointeligente.dto.FuncionarioDTO
import com.gmail.andersoninfonet.pontointeligente.util.SenhaUtil
import com.gmail.andersoninfonet.pontointeligente.dto.CadastroPfDTO
import com.gmail.andersoninfonet.pontointeligente.model.Empresa
import com.gmail.andersoninfonet.pontointeligente.model.enums.Perfil

@Service//Injeção de denpendencia pelo construtor do repositorio do funcionario
class FuncionarioServiceImpl(val funcionarioRepository: FuncionarioRepository) : FuncionarioService {

	@Transactional	
	override fun persistir(funcionario: Funcionario): Funcionario? = funcionarioRepository.save(funcionario)

	override fun buscarPorCPF(cpf: String): Funcionario? = funcionarioRepository.findByCpf(cpf)
	
	override fun buscarPorEmail(email: String): Funcionario? = funcionarioRepository.findByEmail(email)

	override fun buscarPorId(id: Long): Funcionario? = funcionarioRepository.findOne(id)
	
	override fun validarFuncionario(lancamentoDTO: LancamentoDTO, result: BindingResult) {
		if(lancamentoDTO.funcionarioId == null) {
			result.addError(ObjectError("funcionario","Funcionario não informado."))
			return
		}
		
		val funcionario: Funcionario? = buscarPorId(lancamentoDTO.funcionarioId.toLong())
		if(funcionario == null) {
			result.addError(ObjectError("funcionario","Funcionario não encontrado. ID inexistente."))
		}
	}
	
	override fun atualizarDadosFuncionario(funcionario: Funcionario, funcionarioDTO: FuncionarioDTO): Funcionario {
		var senha: String?
		if(funcionarioDTO.senha == null) {
			senha = funcionario.senha
		} else {
			senha = SenhaUtil().criptografar(funcionarioDTO.senha)
		}
		
		val func: Funcionario = Funcionario(funcionarioDTO.nome,funcionarioDTO.email,senha,
					funcionario.cpf,funcionario.perfil,funcionario.empresa,
					funcionarioDTO.valorHora?.toDouble(),
					funcionarioDTO.qtdHorasTrabalhoDia?.toFloat(),
					funcionarioDTO.qtdHorasAlmoco?.toFloat())
		func.id = funcionario.id
		return func
	}

	override fun converterFuncionarioDTO(funcionario: Funcionario): FuncionarioDTO =
		FuncionarioDTO(funcionario.id!!.toString(),funcionario.nome,funcionario.email,"",
		funcionario.valorHora.toString(),funcionario.qtdHorasTrabalhoDia.toString(),
		funcionario.qtdHorasAlmoco.toString())
	
	override fun validarDadosExistentes(cadastroPfDto: CadastroPfDTO, empresa: Empresa?, result: BindingResult) {
		if(empresa == null) {
			result.addError(ObjectError("empresa","Empresa não encontrada."))
		}
		
		val funcionarioCPF:Funcionario? = funcionarioRepository.findByCpf(cadastroPfDto.cpf)
		if(funcionarioCPF != null) {
			result.addError(ObjectError("funcionario","CPF já existente."))
		}
		
		val funcionarioEmail:Funcionario? = funcionarioRepository.findByEmail(cadastroPfDto.email)
		if(funcionarioEmail != null) {
			result.addError(ObjectError("funcionario","Email já existente."))
		}
	}

	override fun converterDtoParaFuncionario(cadastroPfDto: CadastroPfDTO, empresa: Empresa): Funcionario =
		Funcionario(cadastroPfDto.nome, cadastroPfDto.email,SenhaUtil().criptografar(cadastroPfDto.senha),
		cadastroPfDto.cpf,Perfil.ROLE_USUARIO,empresa,cadastroPfDto.valorHora?.toDouble(),
		cadastroPfDto.qtdHorasTrabalhoDia?.toFloat(), cadastroPfDto.qtdHorasAlmoco?.toFloat())

	override fun converterCadastroPfDto(funcionario: Funcionario, empresa: Empresa): CadastroPfDTO =
		CadastroPfDTO(funcionario.id.toString(),funcionario.nome,funcionario.email,"",funcionario.cpf,
		empresa.cnpj,empresa.id.toString(),funcionario.valorHora.toString(),
		funcionario.qtdHorasTrabalhoDia.toString(),funcionario.qtdHorasAlmoco.toString())
}