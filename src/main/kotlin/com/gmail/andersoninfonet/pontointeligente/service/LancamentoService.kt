package com.gmail.andersoninfonet.pontointeligente.service

import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Page
import com.gmail.andersoninfonet.pontointeligente.model.Lancamento
import com.gmail.andersoninfonet.pontointeligente.dto.LancamentoDTO
import org.springframework.validation.BindingResult

interface LancamentoService {
	
	fun buscarPorFuncionarioId(funcionarioId: Long, pageRequest: PageRequest): Page<Lancamento>
	
	fun buscarPorId(id: Long): Lancamento?
	
	fun persistir(lancamento: Lancamento): Lancamento?
	
	fun remover(id: Long)
	
	fun converterDTOParaLancamento(lancamentoDTO: LancamentoDTO, result: BindingResult): Lancamento
	
	fun converteLancamentoDTO(lancamento: Lancamento): LancamentoDTO
}