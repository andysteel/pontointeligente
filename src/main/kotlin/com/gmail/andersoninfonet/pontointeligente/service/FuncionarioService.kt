package com.gmail.andersoninfonet.pontointeligente.service

import com.gmail.andersoninfonet.pontointeligente.model.Funcionario
import com.gmail.andersoninfonet.pontointeligente.dto.LancamentoDTO
import org.springframework.validation.BindingResult
import com.gmail.andersoninfonet.pontointeligente.dto.FuncionarioDTO
import com.gmail.andersoninfonet.pontointeligente.dto.CadastroPfDTO
import com.gmail.andersoninfonet.pontointeligente.model.Empresa

interface FuncionarioService {
	
	fun persistir(funcionario: Funcionario): Funcionario?
	
	fun buscarPorCPF(cpf: String): Funcionario?
	
	fun buscarPorEmail(email: String): Funcionario?
	
	fun buscarPorId(id: Long): Funcionario?
	
	fun validarFuncionario(lancamentoDTO: LancamentoDTO, result: BindingResult)
	
	fun atualizarDadosFuncionario(funcionario:Funcionario,
										  funcionarioDTO: FuncionarioDTO): Funcionario
	
	fun converterFuncionarioDTO(funcionario: Funcionario): FuncionarioDTO
	
	fun validarDadosExistentes(cadastroPfDto:CadastroPfDTO, empresa:Empresa?, result:BindingResult)
	
	fun converterDtoParaFuncionario(cadastroPfDto:CadastroPfDTO,empresa:Empresa): Funcionario
	
	fun converterCadastroPfDto(funcionario:Funcionario,empresa:Empresa): CadastroPfDTO
}