package com.gmail.andersoninfonet.pontointeligente.service.impl

import org.springframework.stereotype.Service
import com.gmail.andersoninfonet.pontointeligente.service.LancamentoService
import com.gmail.andersoninfonet.pontointeligente.repository.LancamentoRepository
import com.gmail.andersoninfonet.pontointeligente.model.Lancamento
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Page
import com.gmail.andersoninfonet.pontointeligente.dto.LancamentoDTO
import org.springframework.validation.BindingResult
import org.springframework.validation.ObjectError
import com.gmail.andersoninfonet.pontointeligente.model.Funcionario
import com.gmail.andersoninfonet.pontointeligente.model.enums.Tipo
import com.gmail.andersoninfonet.pontointeligente.service.FuncionarioService
import com.gmail.andersoninfonet.pontointeligente.repository.FuncionarioRepository
import java.util.Date
import java.text.SimpleDateFormat
import org.springframework.transaction.annotation.Transactional

@Service
class LancamentoServiceImpl(val lancamentoRepository: LancamentoRepository,
							val funcionarioRepository: FuncionarioRepository) : LancamentoService {
	
	private val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
	
	override fun converteLancamentoDTO(lancamento: Lancamento): LancamentoDTO =
			LancamentoDTO(lancamento.id.toString(), dateFormat.format(lancamento.data), lancamento.tipo.toString(), 
						lancamento.descricao,lancamento.localizacao,lancamento.funcionario!!.id.toString())
	
	override fun converterDTOParaLancamento(lancamentoDTO: LancamentoDTO, result: BindingResult): Lancamento {
		
		var id: String? = lancamentoDTO.id
		if(lancamentoDTO.id != null) {
			val lancamento: Lancamento? = buscarPorId(id!!.toLong())
			if(lancamento == null) {
				result.addError(ObjectError("lancamento","Lancamento não encontrado"))
			}
		}
		
		val funcionario: Funcionario? = funcionarioRepository.findOne(lancamentoDTO.funcionarioId!!.toLong())
		if(funcionario == null) {
			result.addError(ObjectError("funcionario", "Funcionario não encontrado"))
		}
		var lancamentoReturn: Lancamento = Lancamento(dateFormat.parse(lancamentoDTO.data),Tipo.valueOf(lancamentoDTO.tipo!!),funcionario!!,lancamentoDTO.descricao,lancamentoDTO.localizacao)
		lancamentoReturn.id = id!!.toLong()
		return lancamentoReturn
	}

	override fun buscarPorFuncionarioId(funcionarioId: Long, pageRequest: PageRequest): Page<Lancamento> {
		val lancamento = lancamentoRepository.findByFuncionarioId(funcionarioId, pageRequest)
		return lancamento
	}

	override fun buscarPorId(id: Long): Lancamento?  = lancamentoRepository.findOne(id)

	@Transactional
	override fun persistir(lancamento: Lancamento): Lancamento? = lancamentoRepository.save(lancamento)

	@Transactional
	override fun remover(id: Long) = lancamentoRepository.delete(id)
	
}
	