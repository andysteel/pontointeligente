package com.gmail.andersoninfonet.pontointeligente.service.impl

import org.springframework.stereotype.Service
import com.gmail.andersoninfonet.pontointeligente.service.EmpresaService
import com.gmail.andersoninfonet.pontointeligente.model.Empresa
import com.gmail.andersoninfonet.pontointeligente.repository.EmpresaRepository
import org.springframework.transaction.annotation.Transactional
import com.gmail.andersoninfonet.pontointeligente.dto.EmpresaDTO
import com.gmail.andersoninfonet.pontointeligente.dto.CadastroPjDTO
import org.springframework.validation.BindingResult
import com.gmail.andersoninfonet.pontointeligente.model.Funcionario
import org.springframework.validation.ObjectError
import com.gmail.andersoninfonet.pontointeligente.repository.FuncionarioRepository
import com.gmail.andersoninfonet.pontointeligente.util.SenhaUtil
import com.gmail.andersoninfonet.pontointeligente.model.enums.Perfil

@Service
class EmpresaServiceImpl(val empresaRepository: EmpresaRepository,
						 val funcionarioRepository: FuncionarioRepository) : EmpresaService{

	override fun buscarPorCnpj(cnpj: String): Empresa? = empresaRepository.findByCnpj(cnpj)
	
	@Transactional
	override fun persistir(empresa: Empresa): Empresa = empresaRepository.save(empresa)
	
	override fun converterEmpresaDTO(empresa: Empresa): EmpresaDTO =
		EmpresaDTO(empresa.id.toString(),empresa.razaoSocial,empresa.cnpj)
	
	override fun validarDadosExistentes(cadastroPjDto: CadastroPjDTO, result: BindingResult) {
		val empresa: Empresa? = empresaRepository.findByCnpj(cadastroPjDto.cnpj)
		if(empresa != null) {
			result.addError(ObjectError("empresa", "Empresa já existente."))
		}
		val funcionarioCPF: Funcionario? = funcionarioRepository.findByCpf(cadastroPjDto.cpf)
		if(funcionarioCPF != null) {
			result.addError(ObjectError("funcionarioCPF","CPF já existente."))
		}
		val funcionarioEmail: Funcionario? = funcionarioRepository.findByEmail(cadastroPjDto.email)
		if(funcionarioEmail != null) {
			result.addError(ObjectError("funcionarioEmail","Email já existente."))
		} 
	}

	override fun converterDtoParaEmpresa(cadastroPjDTO: CadastroPjDTO): Empresa = Empresa(cadastroPjDTO.razaoSocial, cadastroPjDTO.cnpj)

	override fun converterDtoParaFuncionario(cadastroPjDTO: CadastroPjDTO, empresa: Empresa): Funcionario = Funcionario(cadastroPjDTO.nome,
		cadastroPjDTO.email,SenhaUtil().criptografar(cadastroPjDTO.senha),cadastroPjDTO.cpf,Perfil.ROLE_ADMIN,empresa)

	override fun converterCadastroPjDto(funcionario: Funcionario, empresa: Empresa): CadastroPjDTO = CadastroPjDTO(empresa.id.toString(),
		funcionario.nome,funcionario.email,"",funcionario.cpf,empresa.cnpj,empresa.razaoSocial)
}