package com.gmail.andersoninfonet.pontointeligente.util

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder

class SenhaUtil {
	
	fun criptografar(senha: String): String = BCryptPasswordEncoder().encode(senha)
}