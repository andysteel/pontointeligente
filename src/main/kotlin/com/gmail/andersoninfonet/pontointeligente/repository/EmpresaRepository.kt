package com.gmail.andersoninfonet.pontointeligente.repository

import org.springframework.stereotype.Repository
import org.springframework.data.jpa.repository.JpaRepository
import com.gmail.andersoninfonet.pontointeligente.model.Empresa

@Repository
interface EmpresaRepository : JpaRepository<Empresa, Long>{

	fun findByCnpj(cnpj: String): Empresa
}