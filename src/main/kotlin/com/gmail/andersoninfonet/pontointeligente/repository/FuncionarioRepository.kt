package com.gmail.andersoninfonet.pontointeligente.repository

import org.springframework.stereotype.Repository
import org.springframework.data.jpa.repository.JpaRepository
import com.gmail.andersoninfonet.pontointeligente.model.Funcionario

@Repository
interface FuncionarioRepository : JpaRepository<Funcionario, Long> {

	fun findByEmail(email: String): Funcionario
	
	fun findByCpf(cpf: String): Funcionario
}