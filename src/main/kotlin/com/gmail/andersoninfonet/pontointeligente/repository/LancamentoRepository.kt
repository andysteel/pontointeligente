package com.gmail.andersoninfonet.pontointeligente.repository

import org.springframework.stereotype.Repository
import org.springframework.data.jpa.repository.JpaRepository
import com.gmail.andersoninfonet.pontointeligente.model.Lancamento
import com.gmail.andersoninfonet.pontointeligente.model.Funcionario
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Page

@Repository
interface LancamentoRepository : JpaRepository<Lancamento, Long> {
	
	fun findByFuncionarioId(id: Long, pageable: Pageable): Page<Lancamento>
}