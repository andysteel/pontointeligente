package com.gmail.andersoninfonet.pontointeligente.dto

data class EmpresaDTO (
		
	val id: String,
	val razaoSozial: String,
	val cnpj: String
)