package com.gmail.andersoninfonet.pontointeligente.dto

import org.hibernate.validator.constraints.NotEmpty

data class LancamentoDTO (
	
	var id: String? = null,
	
	@get:NotEmpty(message = "Data não pode ser vazia")
	val data: String? = null,
	
	@get:NotEmpty(message = "Tipo não pode ser vazio")
	val tipo: String? = null,
	
	val descricao: String? = "",
	val localizacao: String? = "",
	val funcionarioId: String? = null
)