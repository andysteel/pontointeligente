package com.gmail.andersoninfonet.pontointeligente.dto

import org.hibernate.validator.constraints.NotEmpty
import org.hibernate.validator.constraints.Length
import org.hibernate.validator.constraints.Email

data class FuncionarioDTO (
	
	val id: String? = null,	
	
	@get:NotEmpty(message = "Não pode ser vazio")
	@get:Length(min = 3, max = 200, message = "Nome deve conter entre 3 e 200 caracteres")
	val nome: String? = "",

	@get:NotEmpty(message = "Não pode ser vazio")
	@get:Length(min = 5, max = 200, message = "Nome deve conter entre 5 e 200 caracteres")
	@get:Email(message = "Email invalido")
	val email: String = "",
	
	val senha: String? = "",
	val valorHora: String? = "",
	val qtdHorasTrabalhoDia: String? = "",
	val qtdHorasAlmoco: String? = ""
)