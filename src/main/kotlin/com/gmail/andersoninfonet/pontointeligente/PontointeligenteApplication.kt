package com.gmail.andersoninfonet.pontointeligente

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import com.gmail.andersoninfonet.pontointeligente.repository.EmpresaRepository
import com.gmail.andersoninfonet.pontointeligente.repository.FuncionarioRepository
import org.springframework.boot.CommandLineRunner
import com.gmail.andersoninfonet.pontointeligente.model.Empresa
import com.gmail.andersoninfonet.pontointeligente.model.Funcionario
import com.gmail.andersoninfonet.pontointeligente.util.SenhaUtil
import com.gmail.andersoninfonet.pontointeligente.model.enums.Perfil

@SpringBootApplication
open class PontointeligenteApplication (val empresaRepository: EmpresaRepository,
										val funcionarioRepository: FuncionarioRepository) : CommandLineRunner {
	override fun run(vararg arg0: String?) {
		/*empresaRepository.deleteAll()
		funcionarioRepository.deleteAll()
		
		val empresa = Empresa("Empresa", "10443887000146")
		empresaRepository.save(empresa)
		
		val admin = Funcionario("Admin", "admin@empresa.com", SenhaUtil().criptografar("123456"),
								"09963009700", Perfil.ROLE_ADMIN,empresa)
		funcionarioRepository.save(admin)
		
		val funcionario = Funcionario("Funcionario", "funcionario@empresa.com", SenhaUtil().criptografar("123456"),
									 "09963009700", Perfil.ROLE_USUARIO, empresa)
		funcionarioRepository.save(funcionario)
		
		println("Empresa " + empresa.id)
		println("Admin " + admin.id)
		println("Funcionario " + funcionario.id)*/
	}
}
	
	fun main(args: Array<String>) {
		SpringApplication.run(PontointeligenteApplication::class.java, *args)
	}


